/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.bd;

import com.dieselpoint.norm.Database;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 *
 * @author sergio
 */
@Stateless
public class BaseDatos {
 Database db;
 static String url = "jdbc:postgresql://163.172.96.109:10032/heli?currentSchema=heli";
 static String user = "servivuelos";
static String namedriver = "org.postgresql.Driver";
static String pass = "Q4ZDR7UT4a5t4k";
static String schema = "heli.";

    public BaseDatos() {
       
    
    }
    
    public Database conectar(){
           db = new Database();
           
//    //MySQL       
//     try {
//         Class.forName("com.mysql.jdbc.Driver");
//     } catch (ClassNotFoundException ex) {
//         Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
//     }
//        db.setJdbcUrl("jdbc:mysql://localhost:3306/equifax_reporte?useSSL=false");
//        db.setUser("equifax");
//        db.setPassword("equifax");
//        return db;
//        
        
    //POSTGRES            
        try {
            Class.forName(namedriver);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.setJdbcUrl(url);
        db.setUser(user);
        db.setPassword(pass);
        
        return db;
    }
    
    public String test(){
       
        Database db = this.conectar();
        try{
        List<String> resultado2 = this.db.sql("select CURRENT_TIMESTAMP" ).results(String.class);
        System.out.println(resultado2.size()); 
//        
//        Date date = new Date();
//        date.setTime(resultado2.getTime());
//        String resultado = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
//        
        }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
        return "";
    }
    
    public List<StEjecucciones> listarStEjecuciones(){
        Database db = this.conectar();
        
        
        
        List<StEjecucciones> ejecuciones=null;
        
        try{
         ejecuciones =  db.table(schema+"st_ejecuciones").where("activo = true").results(StEjecucciones.class);
        for (Iterator<StEjecucciones> iterator = ejecuciones.iterator(); iterator.hasNext();) {
            StEjecucciones next = iterator.next();
            System.out.println(next);
        }
        
         }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
        
        return ejecuciones;
    }
    
       public List<StProgramas> listarProgramas(long id_ejecucion){
        Database db = this.conectar();
        List<StProgramas> programas = null;
        try{
            programas = db.table(schema+"st_programas").where("id_ejecucion = ?",id_ejecucion).results(StProgramas.class);
            for (Iterator<StProgramas> iterator = programas.iterator(); iterator.hasNext();) {
                StProgramas next = iterator.next();
                System.out.println(next);
            }
        }catch(Exception e){
            e.printStackTrace();
            db.close();
        }finally{
            db.close();
        }
        
        return programas;
        
                
    }
       
       
      public List<StResultados> listarResultados(long id_programa){
        Database db = this.conectar();
        List<StResultados> resultados = null;
        try{
            resultados = db.table(schema+"st_resultado").where("id_programa = ?",id_programa).results(StResultados.class);
            for (Iterator<StResultados> iterator = resultados.iterator(); iterator.hasNext();) {
                StResultados next = iterator.next();
                System.out.println(next);
            }
        }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
        return resultados;
    } 
       
      
      
      
      
          public List<StDestinatarios> listarDestinatarios(long id_resultado){
        Database db = this.conectar();
        List<StDestinatarios> resultados = null;
        try{
            resultados = db.table(schema+"st_destinatarios").where("id_resultado = ?",id_resultado).results(StDestinatarios.class);
            for (Iterator<StDestinatarios> iterator = resultados.iterator(); iterator.hasNext();) {
                StDestinatarios next = iterator.next();
                System.out.println(next);
            }
        }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
        return resultados;
    }
          
            
      public StConfiguraciones consConfiguraciones(long id_configuracion){
        Database db = this.conectar();
        StConfiguraciones resultados = null;
        try{
            resultados = db.table(schema+"st_configuracion").where("id_configuracion = ?",id_configuracion).first(StConfiguraciones.class);
            
        }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
        return resultados;
    }
      
      
      public List<StConfiguraciones> consConfiguraciones_tipo(long id_tipo){
        Database db = this.conectar();
        List<StConfiguraciones> resultados = null;
        try{
            resultados = db.table(schema+"st_configuracion").where("id_tipo = ? and dinamico = 0",id_tipo).results(StConfiguraciones.class);
            
        }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
        return resultados;
    }
          
     public void update(StEjecucciones st){
         Database db = this.conectar();
         try{
          db.update(st);
         }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
         
     }     
     

  public void update(StProgramas st){
         Database db = this.conectar();
         try{
          db.update(st);
         }catch(Exception e){
            e.printStackTrace();
            db.close();
        
         }finally{
            db.close();
        }
         
         
         
         
     }      
      
      
      
      
      
      
       public Connection getConnection() {
        Connection conn = null;

        try {
            Class.forName(namedriver);
            conn = DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return conn;
    }
      
      
      
      
      
      
        public String update_query(String query) throws Exception{
         
        String q = new String(query);
        
        
        CallableStatement callableS = null;
        Connection con = null;
        String pinNuevo = null;
        String estado = null;
        Integer respuesta = -1;
        String valor = "OK";
        int i = 0;
        try {
            con = this.getConnection();
            callableS = con.prepareCall(q);
          
           
           
            

            callableS.executeUpdate();
    
        
        

        } catch (Exception e) {
           e.printStackTrace();
            valor = "NOK";
        } finally {
            try {
                if (callableS != null) {
                    callableS.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
                 ex.printStackTrace();
            }
        }
        return valor;
    }
            
            
            
       public String select(String query) throws Exception {
      
        String q = query;
      
        CallableStatement callableS = null;
        Connection con = null;
        String pinNuevo = null;
        String estado = null;
        Integer respuesta = -1;
        String valor = "";
        int i = 0;
         String name = "";
        try {
            con = this.getConnection();
            callableS = con.prepareCall(q);

            

            callableS.executeQuery();
            ResultSet r =  callableS.getResultSet();
              ResultSetMetaData rsmd = r.getMetaData();
            int columnCount = rsmd.getColumnCount();

            // The column count starts from 1
           
             
             while (r.next()) {
            
                 for (int is = 1; is <= columnCount; is++ ) {
                    name = name + r.getObject(is)+";";
                 // Do stuff with name
                }    
                 name += "\n";
             }
        
            r.close();


        } catch (Exception e) {
           e.printStackTrace();
            //restartConexion();
        } finally {
            try {
                if (callableS != null) {
                    callableS.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
                 ex.printStackTrace();
            }
        }
        return name;
    }        
    
}
