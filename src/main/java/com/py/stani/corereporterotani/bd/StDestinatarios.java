/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.bd;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sergio
 */
@Stateless
@Table(name ="heli.st_destinatarios")
public class StDestinatarios {
   @Id
   public long id_destinatario;
   @Column(name="id_configuracion")
   public long id_configuracion;
   @Column(name="valor")
   public String valor;
   @Column(name="id_resultado")
   public long id_resultado; 

    @Override
    public String toString() {
        return "StDestinatarios{" + "id_destinatario=" + id_destinatario + ", id_configuracion=" + id_configuracion + ", valor=" + valor + ", id_resultado=" + id_resultado + '}';
    }
   
   
}
