/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.bd;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sergio
 */
@Stateless
@Table(name = "heli.st_configuraciones")
public class StConfiguraciones {
    @GeneratedValue
    public long id_configuracion;
    @Column(name="codigo")
    public String codigo;
    @Column(name = "id_tipo")        
     public long  id_tipo;
    @Column(name = "valor")        
    public String valor;
    @Column(name = "dinamico")
    public long dinamico;

    @Override
    public String toString() {
        return "StConfiguraciones{" + "id_configuracion=" + id_configuracion + ", codigo=" + codigo + ", id_tipo=" + id_tipo + ", valor=" + valor + ", dinamico=" + dinamico + '}';
    }
    
}
