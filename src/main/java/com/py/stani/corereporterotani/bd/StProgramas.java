/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.bd;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sergio
 */
@Stateless
@Table(name="heli.st_programas")
public class StProgramas {
    @Id
    public long id_programa ;
    @Column(name = "id_ejecucion")
    public long id_ejecucion;
    @Column(name ="sql" )
    public String sql;
    @Column(name = "activo")
    public boolean activo;
    @Column(name = "descripcion")
    public String descripcion;
    
    
    @Override
    public String toString() {
        return "StProgramas{" + "id_programa=" + id_programa + ", id_ejecucion=" + id_ejecucion + ", sql=" + sql + ", activo=" + activo + ", descripcion=" + descripcion + '}';
    }
    
    
}
