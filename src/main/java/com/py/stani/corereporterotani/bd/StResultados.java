/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.bd;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sergio
 */
@Stateless
@Table(name = "heli.st_resultados")
public class StResultados {
   @Id
   public long id_resultado;
   @Column(name = "id_programa")
   public long id_programa;
   @Column(name = "id_tipo_salida")
   public long id_tipo_salida;

    @Override
    public String toString() {
        return "StResultados{" + "id_resultado=" + id_resultado + ", id_programa=" + id_programa + ", id_tipo_salida=" + id_tipo_salida + '}';
    }
   
   
}
