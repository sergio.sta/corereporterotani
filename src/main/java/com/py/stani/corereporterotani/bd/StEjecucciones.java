/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.bd;

import java.sql.Date;
import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sergio
 * SELECT id_ejecucion, descripcion, inicio, fin, proxima_ejecucion, activo, tipo_periodo, valor
FROM rtp.st_ejecuciones;

 */
@Stateless
@Table(name="heli.st_ejecuciones")
public class StEjecucciones {
    @Id
    public long id_ejecucion;
    @Column(name="descripcion")
    public String descripcion;
    @Column(name="inicio")
    public java.sql.Timestamp inicio;
    @Column(name="fin")
    public java.sql.Timestamp fin;
    @Column(name="proxima_ejecucion")
    public java.sql.Timestamp proxima_ejecucion;
    @Column(name="activo")
    public boolean activo;
    @Column(name="tipo_periodo")
    public int tipo_periodo;
    @Column(name="valor")
    public int valor;

    @Override
    public String toString() {
        return "StEjecucciones{" + "id_ejecucion=" + id_ejecucion + ", descripcion=" + descripcion + ", inicio=" + inicio + ", fin=" + fin + ", proxima_ejecucion=" + proxima_ejecucion + ", activo=" + activo + ", tipo_periodo=" + tipo_periodo + ", valor=" + valor + '}';
    }
    
    
    
}
