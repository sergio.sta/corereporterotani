/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.timer;

import com.py.stani.corereporterotani.bd.BaseDatos;
import com.py.stani.corereporterotani.bd.StEjecucciones;
import com.py.stani.corereporterotani.timer.entrada.ProcesoProgramas;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sergio
 */
@Stateless
public class CoreProcesoIterativo {
  Logger log = LogManager.getLogger(this.getClass());
    @Schedule(month = "*", hour = "*", dayOfMonth = "*", year = "*", minute = "*/10", second = "0", persistent = false)
    
    public void myTimer() {
        this.log.info("-----------------------------------------------------------");
        
        this.log.info("----------------INICIO DE PROCESO REPORTERO--------------------------------");
        this.log.info("-----------------------------------------------------------");
        BaseDatos bd = new BaseDatos();
        try{
            String resultado = bd.test();

            if(resultado!= null){
            this.log.info("Conexion exitosa "+resultado);
            }else{
                this.log.error("Problemas con la conexion de la base de datos.. verificar");
                return;
            }
        }catch(Exception e){
            e.printStackTrace();
            this.log.error("FALLO DE CONEXION A LA BASE");
            return;
        }
        
        List<StEjecucciones> lejecuciones = bd.listarStEjecuciones();
        
        
        for (Iterator<StEjecucciones> iterator = lejecuciones.iterator(); iterator.hasNext();) {
            StEjecucciones next = iterator.next();
            //AQUI PONER CONTROL DE FECHA
            Timestamp ts=new Timestamp(System.currentTimeMillis());
            this.log.info(next.proxima_ejecucion +":++:"+ts );
            if(next.activo && next.proxima_ejecucion.before(ts)){
                if(next.inicio!=null && next.inicio.before(ts)) continue;
                if(next.fin!=null && next.fin.after(ts)) continue;
                
               
                        this.log.info(next);
                        ProcesoProgramas p = new ProcesoProgramas(next.id_ejecucion,next);

                        p.start();


                
            }
            
        }
        
        
        
        this.log.info("-----------------------------------------------------------");
        
        this.log.info("----------------FIN DE PROCESO--------------------------------");
        this.log.info("-----------------------------------------------------------");
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
