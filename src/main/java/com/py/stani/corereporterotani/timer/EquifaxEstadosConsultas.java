/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.timer;

import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *@Table(name="people")
public class Person {
	@Id
	@GeneratedValue
	public long personId;

	public String name;

	@Column(name = "theColumnName")
	public String renameThis;

	@Transient
	public String thisFieldGetsIgnored;
}

 * @author sergio
 */
@Stateless
@Table(name="equifax_estados_consultas")
public class EquifaxEstadosConsultas {
    	@Id
	public long id_estado;


        @Column(name = "descripcion")
        public String descripcion;
        
            @Override
    public String toString() {
        return "EquifaxEstadosConsultas{" + "id_estado=" + id_estado + ", descripcion=" + descripcion + '}';
    }
}
