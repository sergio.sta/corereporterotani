/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.timer.salida;

import com.py.stani.corereporterotani.bd.BaseDatos;
import com.py.stani.corereporterotani.bd.StConfiguraciones;
import com.py.stani.corereporterotani.bd.StDestinatarios;
import com.py.stani.corereporterotani.bd.StResultados;
import com.py.stani.corereporterotani.utiles.Utiles;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;  
  
import javax.activation.*;  
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author sergio
 */
public class ProcesoSalida {
        Logger log = LogManager.getLogger(this.getClass());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
         private static String userMail;
         private static String passMail;
        
         private class SMTPAuthenticator extends javax.mail.Authenticator {

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            String user = userMail;
            String pass = passMail;
            return new PasswordAuthentication(user, pass);
        }
    }

        
    public void procesar(long id_programa,String path){
        BaseDatos bd = new BaseDatos();
        List<StResultados> lresultado = bd.listarResultados(id_programa);
        for (Iterator<StResultados> iterator = lresultado.iterator(); iterator.hasNext();) {
            StResultados next = iterator.next();
             this.log.debug(next);   
            //ACA TRAER LOS PARAMETROS
           List<StDestinatarios> ldestinatario =   bd.listarDestinatarios(next.id_resultado);
            HashMap<String,String> conf = new HashMap<String, String>();
              for (Iterator<StDestinatarios> iterator1 = ldestinatario.iterator(); iterator1.hasNext();) {
                StDestinatarios next1 = iterator1.next();
                conf.put(bd.consConfiguraciones(next1.id_configuracion).codigo, next1.valor);
                log.debug(next1);
            }
               
               switch(Integer.parseUnsignedInt(next.id_tipo_salida+"")){
                case Utiles.ARCHIVO: 
                    File entrada = new File(path);
                    String nombre = conf.get(Utiles.ARCHIVO_NOMBRE_ARCHIVO).replace(Utiles.FECHA, sdf.format(new Date())).replace(Utiles.FECHA_HORA, sdf2.format(new Date()));
                    File salida = new File(conf.get(Utiles.ARCHIVO_PATH_DESTINO)+nombre);
                    
                    try {
                      FileUtils.copyFile(entrada, salida);
                    }catch(Exception e){
                        this.log.error("ERROR DE COPIADO",e);
                        
                    }
                    
                    break;
                case Utiles.MAIL:
                    
                     entrada = new File(path);
                     nombre = conf.get(Utiles.ARCHIVO_NOMBRE_ARCHIVO).replace(Utiles.FECHA, sdf.format(new Date())).replace(Utiles.FECHA_HORA, sdf2.format(new Date()));
                     salida = new File(conf.get(Utiles.ARCHIVO_PATH_DESTINO)+nombre);
                    
                    try {
                      FileUtils.copyFile(entrada, salida);
                    }catch(Exception e){
                        this.log.error("ERROR DE COPIADO",e);
                        
                    }
                    
                    List<StConfiguraciones> lconfmail =   bd.consConfiguraciones_tipo(next.id_tipo_salida);
                     HashMap<String,String> confmail = new HashMap<String, String>();
                    for (Iterator<StConfiguraciones> iterator1 = lconfmail.iterator(); iterator1.hasNext();) {
                        StConfiguraciones next1 = iterator1.next();
                        this.log.info(next1.codigo+":::"+ next1.valor);
                        confmail.put(next1.codigo, next1.valor);
                    }
                    
                    try{
                           // Defines the E-Mail information.
                           String from =confmail.get(Utiles.MAIL_ORIGEN);
                           String to = conf.get(Utiles.MAIL_DESTINOS);
                           String subject = conf.get(Utiles.MAIL_TITULO);;
                           String bodyText = conf.get(Utiles.MAIL_TEXTO);;

                           // The attachment file name.ls
                           String attachmentName = conf.get(Utiles.ARCHIVO_PATH_DESTINO)+nombre;

                           // Creates a Session with the following properties.
                           Properties props = new Properties();
                           
                           /*
                           
                              
            
                           */
                           
                           
                           props.put("mail.smtp.host", confmail.get(Utiles.MAIL_IP));
                           props.put("mail.transport.protocol", "smtp");
                            props.put("mail.debug", "true");
                           props.put("mail.smtp.starttls.enable", "true");
                            props.put("mail.smtp.auth", "true");
                            props.put("mail.smtp.socketFactory.fallback", "true");
                           
                           props.put("mail.smtp.port", confmail.get(Utiles.MAIL_PUERTO));
                          // Session session = Session.getDefaultInstance(props);
                          
                          
                          this.userMail = confmail.get(Utiles.MAIL_USER);
                          this.passMail = confmail.get(Utiles.MAIL_PASS);
                           Authenticator auth = new SMTPAuthenticator();
                           Session session = Session.getInstance(props, auth);
                           
                           try {
                               MimeMessage msg = new MimeMessage(session);
                               InternetAddress fromAddress = new InternetAddress(from);
                               this.log.info("mail.smtp.host:"+props.getProperty("mail.smtp.host"));
                               this.log.info("mail.transport.protocol:"+props.getProperty("mail.transport.protocol"));
                               this.log.info("mail.smtp.starttls.enable:"+props.getProperty("mail.smtp.starttls.enable"));
                                this.log.info("mail.smtp.port:"+props.getProperty("mail.smtp.port"));
                                 this.log.info("fromAddress:"+fromAddress);
                              this.log.info("MAIL_USER:"+confmail.get(Utiles.MAIL_USER));
                              this.log.info("MAIL_PASS:"+confmail.get(Utiles.MAIL_PASS));
                               String[] destinos = to.split(",");
                               
                               for (int i = 0; i < destinos.length; i++) {
                                   
                               
                               InternetAddress toAddress = new InternetAddress(destinos[i]);
                               msg.addRecipient(Message.RecipientType.TO, toAddress);
                               // Create an Internet mail msg.
                              }
                               
                               msg.setFrom(fromAddress);
                               
                               msg.setSubject(subject);
                               msg.setSentDate(new Date());

                               // Set the email msg text.
                               MimeBodyPart messagePart = new MimeBodyPart();
                               messagePart.setText(bodyText);

                               // Set the email attachment file

                               FileDataSource fileDataSource = new FileDataSource(attachmentName);

                               MimeBodyPart attachmentPart = new MimeBodyPart();
                               attachmentPart.setDataHandler(new DataHandler(fileDataSource));
                               attachmentPart.setFileName(fileDataSource.getName());

                               // Create Multipart E-Mail.
                               Multipart multipart = new MimeMultipart();
                               multipart.addBodyPart(messagePart);
                               multipart.addBodyPart(attachmentPart);

                               msg.setContent(multipart);

                               // Send the msg. Don't forget to set the username and password
                               // to authenticate to the mail server.
                               Transport.send(msg, confmail.get(Utiles.MAIL_USER), confmail.get(Utiles.MAIL_PASS));
                           } catch (Exception e) {
                               e.printStackTrace();
                           }

                       } catch (Exception e) {
                           this.log.error("ERROR DE MAIL", e);
                           }
                           break;
                       
                case Utiles.FTP: 
                    FTPClient client = new FTPClient();
                    FileInputStream fis = null;

                    try {
                        client.connect("mydomain.com");
                        client.login("user", "password");

                        String filename = "myPDF.pdf";
                        fis = new FileInputStream(filename);

                        client.storeFile("temp.pdf", fis);
                        fis.close();
                        client.logout();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                
               }
        
        }
    }
}
