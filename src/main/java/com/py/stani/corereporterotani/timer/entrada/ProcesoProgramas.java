/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.py.stani.corereporterotani.timer.entrada;

import com.py.stani.corereporterotani.bd.BaseDatos;
import com.py.stani.corereporterotani.bd.StEjecucciones;
import com.py.stani.corereporterotani.bd.StProgramas;
import com.py.stani.corereporterotani.bd.StResultados;
import com.py.stani.corereporterotani.timer.salida.ProcesoSalida;
import com.py.stani.corereporterotani.utiles.Utiles;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sergio
 */
public class ProcesoProgramas extends Thread{
    Logger log = LogManager.getLogger(this.getClass());
    StEjecucciones ejecucion;
    long id_ejecucion;
    public ProcesoProgramas(long id_ejecucion, StEjecucciones ejecucion) {
     this.id_ejecucion = id_ejecucion;
     this.ejecucion = ejecucion;
    }
    
     @Override
    public void run(){
        ProcesoSalida salida = new ProcesoSalida();
        BaseDatos bd = new BaseDatos();
        //se hace asi porque tarda demasiado.
         this.actualizarProceso(bd);
        //DESACTIVAR LA EJECUCION POR SI EXPLOTA ..SI TODO SALE BIEN SE ACTUALIZA LA FECHA Y SE ACTIVA OTRA VEZ
        this.log.info("Buscando los programas de id ejecucion "+ id_ejecucion);
        List<StProgramas> lprogramas =  bd.listarProgramas(id_ejecucion);
        // List<StResultados> lresultados =  bd.listarResultados(id_ejecucion);
        
        //ACA LLAMAR AL METODO PURETE DE SQL GENERICO
         for (Iterator<StProgramas> iterator = lprogramas.iterator(); iterator.hasNext();) {
             StProgramas next = iterator.next();
             if(!next.activo) continue;
             try{
                String resultadosql =  bd.select(next.sql);
                this.log.info(resultadosql);
               

                File myfile = new File(  Utiles.PATH .replaceAll("<ejecucion>", next.id_ejecucion+"").replaceAll("<programa>", next.id_programa+""));

                FileUtils.writeStringToFile(myfile, resultadosql, 
                        StandardCharsets.UTF_8.name());

                resultadosql = null;
                
                salida.procesar(next.id_programa,Utiles.PATH.replaceAll("<ejecucion>", next.id_ejecucion+"").replaceAll("<programa>", next.id_programa+""));
                
             }catch(Exception e){
                 e.printStackTrace();
                 this.log.error("FALLO EL PROGRAMA"+next.toString());
                 next.activo = false;
                 bd.update(next);
                 return;
             }
         }
         
        
         
         
    }

    private void actualizarProceso(BaseDatos bd) {
        Calendar calendar = Calendar.getInstance();
       
        calendar.setTime(new Date(this.ejecucion.proxima_ejecucion.getTime()));
       switch(this.ejecucion.tipo_periodo){
           case Utiles.MINUTO:
               calendar.add(Calendar.MINUTE, this.ejecucion.valor);
               this.ejecucion.proxima_ejecucion = new Timestamp( calendar.getTimeInMillis());
              
           break;
           case Utiles.HORA:
               calendar.add(Calendar.HOUR, this.ejecucion.valor);
               this.ejecucion.proxima_ejecucion = new Timestamp( calendar.getTimeInMillis());
           break;
           case Utiles.DIA:
               calendar.add(Calendar.DATE, this.ejecucion.valor);
               this.ejecucion.proxima_ejecucion = new Timestamp( calendar.getTimeInMillis());
           break;
           case Utiles.MES:
                  calendar.add(Calendar.MONTH, this.ejecucion.valor);
               this.ejecucion.proxima_ejecucion = new Timestamp(calendar.getTimeInMillis());
           break;
           case Utiles.ANHO:
                  calendar.add(Calendar.YEAR, this.ejecucion.valor);
               this.ejecucion.proxima_ejecucion = new Timestamp( calendar.getTimeInMillis());
           break;
       }
       bd.update(ejecucion);
    }
    
}
